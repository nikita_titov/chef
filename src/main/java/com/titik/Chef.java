package com.titik;

import java.lang.reflect.Constructor;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Chef {
    private String name;
    private Salat salat;

    Chef() {
        name = "Nikita";
        salat = new Salat();
    }

    public Chef(String name, Salat salat) {
        this.name = name;
    }

    private Vegetable getIngredient(Scanner scanner) {
        String ingredientName;


        System.out.println("Enter the ingredient name:");
        ingredientName = scanner.next();


        try {
            ingredientName = "com.titik.FamilyVegetable." + ingredientName;
            Constructor ingredientClass = Class.forName(ingredientName).getConstructor();
            Vegetable vegetable = (Vegetable) ingredientClass.newInstance();

            return vegetable;

        } catch (Exception e) {
            System.out.println(e);
            System.out.println("No such ingredient!");

            return null;
        }
    }


    void showOptions() {
        Scanner scanner = new Scanner(System.in);
        int choice = -1;
        Vegetable vegetable = null;

        System.out.println("Hello! My name is " + name + ".");
        System.out.println("What would you like?");


        while (choice != 0) {
            System.out.println("\nChoose one of the options:");
            System.out.println("1. Name Salat");
            System.out.println("2. Show recipe");
            System.out.println("3. Add ingredient");
            System.out.println("4. Sort ingredients by calories");
            System.out.println("5. Get ingredients for calories");
            System.out.println("0. Exit");


            try {
                choice = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Wrong option!");
                scanner.next();
                choice = -1;
            }


            switch (choice) {
                case 1:
                    System.out.println("Name your salad:");
                    salat.setname(scanner.next());
                    break;

                case 2:
                    salat.showRecipe();
                    break;

                case 3:
                    vegetable = getIngredient(scanner);
                    if (vegetable != null) {
                        if (!salat.addIngrediants(vegetable)) {
                            System.out.println("Cannot add ingredient!");
                        }
                    }
                    break;

                case 4:
                    salat.sortIngredientsByCalories();
                    break;
                case 5:
                    salat.showIngredientsForCalories(scanner);
                    break;
                case 0:
                    System.exit(0);
                    break;

                default:
                    break;
            }
        }

        scanner.close();
    }


}