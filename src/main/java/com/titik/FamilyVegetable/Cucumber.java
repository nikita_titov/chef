package com.titik.FamilyVegetable;

public class Cucumber extends SemeistvoTikva {
    public Cucumber() {
        super("Сucumber", 37);
    }

    public Cucumber(String name, double kcal) {
        super(name, kcal);
    }

    public Cucumber(String name) {
        super(name);
    }
}
