package com.titik.FamilyVegetable;

import com.titik.Vegetable;

class SemeistvoPaslenovi extends Vegetable {
    SemeistvoPaslenovi(String name, double kcal) {
        super(name, kcal);
        setCategory(" Family Paslenovii ");
    }
    SemeistvoPaslenovi(String name) {
        super(name);
    }
}
