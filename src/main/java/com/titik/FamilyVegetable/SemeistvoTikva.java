package com.titik.FamilyVegetable;

import com.titik.Vegetable;

class SemeistvoTikva extends Vegetable {
    SemeistvoTikva(String name, double kcal) {
        super(name, kcal);
        setCategory(" Family Tikva ");
    }

    SemeistvoTikva(String name) {
        super(name);
    }
}