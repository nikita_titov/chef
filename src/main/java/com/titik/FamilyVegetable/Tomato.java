package com.titik.FamilyVegetable;

public class Tomato extends SemeistvoPaslenovi {
    public Tomato() {
        super("Tomato", 60);
    }

    public Tomato(String name, double kcal) {
        super(name, kcal);
    }

    public Tomato(String name) {
        super(name);
    }
}
