package com.titik;

import java.util.*;

public class Salat {
    private String name;
    private List<Vegetable> ingredients = new ArrayList<>();

    Salat() {
        name = "Nun";
    }

    public String getName() {
        return name;
    }

    public List<?> getIngredient() {
        return ingredients;
    }

    void setname(String name) {
        ingredients.clear();
        this.name = name;
    }

    boolean addIngrediants(Vegetable vegetable) {
        return ingredients.add(vegetable);
    }

    void showRecipe() {
        if (ingredients.isEmpty()) {

            System.out.println("You need add ingredients!");
        }

        System.out.println("The salat " + name + " contains:");

        for (Vegetable vegetable : ingredients) {
            System.out.println(vegetable.toString());
        }

        System.out.println("----------------------------");
        System.out.println("Total energy: " + countCalories() + "kcal");
    }

    private double countCalories() {
        double calories = 0.0;

        for (Vegetable vegetable : ingredients) {

            calories += vegetable.getCalories();
        }

        return calories;
    }

    double sortIngredientsByCalories() {
        double calories = 0.0;
        ingredients.sort(new CaloriesComparator());
        ingredients.forEach(i -> System.out.println(i.toString()));

        return calories;
    }

    void showIngredientsForCalories(Scanner scanner) {
        double lowerCalories, upperCalories;

        System.out.println("Enter the lower limit:");

        lowerCalories = scanner.nextDouble();

        System.out.println("Enter the upper limit:");

        upperCalories = scanner.nextDouble();

        showIngredientsByCalories(lowerCalories, upperCalories);
    }

    private void showIngredientsByCalories(double lowerCalories, double upperCalories) {
        double calories;

        System.out.println("Ingredients for calories ["
                + lowerCalories + ", " + upperCalories + "]");
        for (Vegetable vegetable : ingredients) {
            calories = vegetable.getCalories();
            if (calories >= lowerCalories && calories <= upperCalories) {
                System.out.println(vegetable.getName() + ", "
                        + vegetable.getCalories());
            } else {
                System.out.println("No such ingredients!");
            }
        }
    }
}
