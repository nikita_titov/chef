package com.titik;

abstract public class Vegetable {

    private String name;
    private String category;
    private double kcal;

    public Vegetable(String name) {
        this.name = name;
    }

    public Vegetable(String name, double calories) {
        this.name = name;
        this.kcal = calories;
    }

    protected Vegetable() {
    }

    public String toString() {
        return (name + "(" + category + ")," + getCalories() + "kcal");
    }

    String getName() {
        return name;
    }

    double getCalories() {
        return kcal;
    }

    protected void setCategory(String category) {
        this.category = category;
    }
}
